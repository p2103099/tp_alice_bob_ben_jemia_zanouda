package entree;

import java.util.ArrayList;

public class Personne implements Entree {
    String nom;
    String[] prenoms;
    String fonction;
    Genre genre;
    Personne conjoint;
    Societe societe;

    
    public Personne(String nom, String[] prenoms, String fonction, Genre genre, Personne conjoint, Societe societe) {
        this.nom = nom;
        this.prenoms = prenoms;
        this.fonction = fonction;
        this.genre = genre;
        this.conjoint = conjoint;
        this.societe = societe;
    }

    
    public Boolean recherche(String elem) {

        return null;
    }
    

    public String prenomsToString(ArrayList<String> arraylist) {
        String result = "";
        for (String prenom: arraylist) {
            result += prenom + " ";
        }
        return result;
    }

    
    public String toString(Entree.Presentation pres, Entree.Sens sens) {
        ArrayList<String> prenoms_initiales_sauf_premier = new ArrayList<String>();
        ArrayList<String> prenoms_entiers = new ArrayList<String>();
        for (String prenom : prenoms) {
            prenoms_initiales_sauf_premier.add(prenom.charAt(0)+".");
            prenoms_entiers.add(prenom);
        }
        prenoms_initiales_sauf_premier.remove(0);


        if (sens ==Entree.Sens.PRENOMS_NOM) {
            switch (pres) {
                case SIMPLE:
                    return (this.genre==Entree.Genre.FEMME) ? "Mme. " : "M. " + this.prenoms[0] + " " + prenomsToString(prenoms_initiales_sauf_premier) + this.nom + " ("+this.societe+")";
                case COMPLET:
                    return (this.genre==Entree.Genre.FEMME) ? "Mme. " : "M. " + prenomsToString(prenoms_entiers) + " " + this.nom + "\n    - Société: " + this.societe + "\n    - Fonction: " + this.fonction;
                default:
                    return this.prenoms[0].charAt(0) + ". " + this.nom;
            }
        }
        switch (pres) {
            case SIMPLE:
                return (this.genre==Entree.Genre.FEMME) ? "Mme. " : "M. " + this.nom + " " + this.prenoms[0] + prenomsToString(prenoms_initiales_sauf_premier) + " ("+this.societe+")";
            case COMPLET:
                return (this.genre==Entree.Genre.FEMME) ? "Mme. " : "M. " + this.nom + " " + prenomsToString(prenoms_entiers) + "\n    - Société: " + this.societe + "\n    - Fonction: " + this.fonction;
            default:
                return this.nom + this.prenoms[0].charAt(0) + ". ";
        }

    }


    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String[] getPrenoms() {
        return this.prenoms;
    }

    public void setPrenoms(String[] prenoms) {
        this.prenoms = prenoms;
    }

    public String getFonction() {
        return this.fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public Genre getGenre() {
        return this.genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Personne getConjoint() {
        return this.conjoint;
    }

    public void setConjoint(Personne conjoint) {
        this.conjoint = conjoint;
    }

    public Societe getSociete() {
        return this.societe;
    }

    public void setSociete(Societe societe) {
        this.societe = societe;
    }



}