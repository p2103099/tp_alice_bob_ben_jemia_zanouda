package entree;

public interface Entree {
    public enum Sens{NOM_PRENOMS, PRENOMS_NOM}
    public enum Presentation{ABREGE,SIMPLE,COMPLET}
    public enum Genre{HOMME,FEMME}
}