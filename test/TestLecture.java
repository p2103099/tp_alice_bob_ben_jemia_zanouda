package test;

import carnet.*;
import entree.*;
import entree.Entree.*;

public class TestLecture {

    public static void main(String[] args) {
        Carnet c = new Carnet();
        c.lectureFichier("test.txt");
        Entree[] liste = c.getEntrees();
        for (Entree e : liste) {
            if(e instanceof Personne) {
                Personne p = (Personne) e;
                System.out.println(p.toString(Presentation.COMPLET, Sens.NOM_PRENOMS));
            }
            else
                System.out.println(e);
        }
    }
    
}
