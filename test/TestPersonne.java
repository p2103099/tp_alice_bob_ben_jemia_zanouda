package test;

import entree.Entree;
import entree.Personne;

import entree.Societe;
import entree.Entree.Presentation;
import entree.Entree.Sens;

public class TestPersonne {
    public static void main(String[] args) {
        Societe samsung = new Societe("SAMSUNG ELECTRONICS");
        Societe bfm_tv = new Societe("BFM TV");

        Personne tiziano = null;
        Personne manuel = new Personne("GUILLAND",new String[] {"Manuel","Georges","Hamdel"},"Directeur",Entree.Genre.HOMME,tiziano,samsung);
        tiziano = new Personne("MANZERUCCI",new String[] {"Tiziano","Pietro"}, "Stagiaire", Entree.Genre.HOMME, manuel, bfm_tv);


        System.out.println(tiziano.toString(Presentation.ABREGE, Sens.PRENOMS_NOM) + "\n");
        System.out.println(manuel.toString(Presentation.COMPLET, Sens.NOM_PRENOMS) + "\n");
        System.out.println(tiziano.toString(Presentation.SIMPLE, Sens.PRENOMS_NOM) + "\n");
    }
}
