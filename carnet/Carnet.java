package carnet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import entree.*;
import entree.Entree.Genre;

public class Carnet {

    private Entree[] entrees;
    private Entree[] selectionnées;
    

    public void lectureFichier(String nomFichier) {

        String type;
        HashMap<Entree, Integer[]> personnes = new HashMap<>();
        
        // Read file line per line
        try {
            BufferedReader br = new BufferedReader(new FileReader(nomFichier));
            String line;
            
            while ((line = br.readLine()) != null) {
                // Split line into type and content
                String[] split = line.split(";");
                type = split[1];
                // Create new Entree
                Entree entree = null;
                int conjoint = -1;
                int societe = -1;
                switch (type) {
                    case "PERSONNE":
                        String[] prenoms = split[2].split(",");
                        String genre = split[3];
                        Genre g = null;
                        switch(genre) {
                            case "H":
                                g = Entree.Genre.HOMME;
                                break;
                            case "F":
                                g = Entree.Genre.FEMME;
                                break;
                        }
                        if(split[5].equals("")) {
                            conjoint = -1;
                        } else {
                            conjoint = Integer.parseInt(split[5]);
                        }
                        if(split[6].equals("")) {
                            societe = -1;
                        } else {
                            societe = Integer.parseInt(split[6]);
                        }
                        entree = new Personne(split[3], prenoms, split[7], g, null, null); // Object Personne et Société au lieu de String
                        
                        break;
                    case "SOCIETE":
                        entree = new Societe(split[2]);
                        break;
                    default:
                        System.out.println("Type inconnu: " + type);
                        break;
                }
                // Add Entree to array
                if (entree != null) {
                    personnes.put(entree, new Integer[]{conjoint, societe});
                }
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Erreur de lecture du fichier: " + e.getMessage());
        }
        entrees = new Entree[personnes.size()];
        int i = 0;
        for(Entree e : personnes.keySet()) {
            if(e instanceof Personne){
                Personne p = (Personne) e;
                if(personnes.get(e)[0] != -1) {
                    p.setConjoint((Personne) entrees[personnes.get(e)[0]-1]);
                }
                if(personnes.get(e)[1] != -1) {
                    p.setSociete((Societe) entrees[personnes.get(e)[1]-1]);
                }
                e = p;
            }
            entrees[i] = e;
            i++;
        }
    }

    public Entree[] getEntrees() {
        return entrees;
    }
}
